#!/bin/sh -x

echo lcd DEC_Guide/tmp/en-US/html >tmp.ftp
echo cd /var/www/arpsc/downloads >>tmp.ftp
#echo mkdir DEC_Guide >>tmp.ftp
#echo mkdir DEC_Guide/Common_Content >>tmp.ftp
#echo mkdir DEC_Guide/Common_Content/images >>tmp.ftp
#echo mkdir DEC_Guide/Common_Content/css >>tmp.ftp
#echo mkdir DEC_Guide/images >>tmp.ftp
echo cd DEC_Guide >>tmp.ftp
echo 'mput *' >>tmp.ftp
echo cd images >>tmp.ftp
echo lcd images >>tmp.ftp
echo 'mput *' >>tmp.ftp
echo cd ../Common_Content/css >>tmp.ftp
echo lcd ../Common_Content/css >>tmp.ftp
echo 'mput *' >>tmp.ftp
echo cd ../images >>tmp.ftp
echo lcd ../images >>tmp.ftp
echo 'mput *' >>tmp.ftp
echo bye >>tmp.ftp
ftp -i ftp.mi-nts.org <tmp.ftp
#rm tmp.ftp
