#!/bin/sh -x
NAME=DEC_Guide
rm -Rf $NAME
publican create --product=mini-SET \
    --version=10.1 \
    --brand=arpsc \
    --lang=en-US \
    --name=$NAME \
    --type=article


pushd $NAME

sed -i "{s?| You need to change the HOLDER entity in the en-US/DEC_Guide.ent file |?the Michigan Section of the American Radio Relay League?}" en-US/DEC_Guide.ent
sed -i "{s/>DEC_Guide</>DEC Guide</}" en-US/Article_Info.xml
sed -i "{s/short description/A guide to the upcoming mini-SET for DECs and ECs/}" en-US/Article_Info.xml

publican build --formats=html --langs=en-US
seamonkey ./tmp/en-US/html/index.html &
popd
